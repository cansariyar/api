<?php
	include "settings.php";
    header("Content-Type: application/json; charset=utf-8");
	$islem = isset($_GET["islem"]) ? addslashes(trim($_GET["islem"])) : null;
	$jsonArray = array();

	$_code = 200;
	if($_SERVER['REQUEST_METHOD'] == "POST")
	{
        $name = addslashes($_POST["name"]);
        $stock = addslashes($_POST["stock"]);
        $data = Array (
            'name' => $name,
            'stock' => $stock,
	        'create_date' => Date('Y-m-d H:i:s')
        );
        $status = $db->insert('product', $data);
        if($status){
            $jsonArray["code"] = 0;
            $jsonArray["msg"] = "success";
            $jsonArray["data"]["product_id"] = str_pad($db->getInsertId(), 10, '0', STR_PAD_LEFT);
            $jsonArray["data"]["name"] = $name;
            $jsonArray["data"]["stock"] = $stock;
            $jsonArray["data"]["create_date"] = Date('Y-m-d H:i:s');
        }else {
            $jsonArray["code"] = 500;
            $jsonArray["msg"] = $db->getLastError();
        }

	}
	else if($_SERVER['REQUEST_METHOD'] == "GET") {
        $jsonArray["code"] = 0;
        $jsonArray["msg"] = "success";
        $products = $db->get("product");
        foreach ($products as $k){
            $jsonArray["data"][] = $k;
        }
	}else {
		$jsonArray["code"] = 500;
		$jsonArray["msg"] = "Geçersiz method!";
	}

    echo json_encode($jsonArray);
?>
